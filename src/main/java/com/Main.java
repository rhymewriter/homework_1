package main.java.com;

import main.java.com.conditionalOperators.ConditionOperatorsTask;
import main.java.com.cycles.CyclesTask;
import main.java.com.functions.DayNameByNumber;
import main.java.com.functions.NumberAsString;
import main.java.com.functions.Pifagor;
import main.java.com.oneDimensionalArrays.ArrayTask;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        System.out.println("condition operator task" + "\n" + "-------------------------------");
        System.out.println("1. Если а – четное посчитать а*б, иначе а+б");
        ConditionOperatorsTask conditionOperatorsTask = new ConditionOperatorsTask();
        int taskOne = conditionOperatorsTask.getResult(3,10);
        System.out.println(taskOne);
        System.out.println("2. Определить какой четверти принадлежит точка с координатами (х,у)");
        String taskTwo = conditionOperatorsTask.getQuarterNumber(-10, 3);
        System.out.println(taskTwo);
        System.out.println("3. Найти суммы только положительных из трех чисел");
        int taskThree = conditionOperatorsTask.getSumOfPositiveNumbers(-3,5,7);
        System.out.println(taskThree);
        System.out.println("4. Посчитать выражение (макс(а*б*с, а+б+с))+3");
        int taskFour = conditionOperatorsTask.getResultOfExpression(6,3,2);
        System.out.println(taskFour);
        System.out.println("5. Написать программу определения оценки студента по его рейтингу");
        String taskFive = conditionOperatorsTask.getGrade(74);
        System.out.println(taskFive);

        System.out.println("\n" + "cycles" + "\n" + "-------------------------------");
        CyclesTask cyclesTask = new CyclesTask();
        System.out.println("1. Найти сумму четных чисел и их количество в диапазоне от 1 до 99");
        System.out.println(cyclesTask.sumNumbersAndQuantity());
        System.out.println("2. Проверить простое ли число?");
        System.out.println(cyclesTask.simpleNumber(21));
        System.out.println("3. Найти корень натурального числа с точностью до целого");
        System.out.println(cyclesTask.sqrtOfNumber(21));
        System.out.println("4. Вычислить факториал числа n. n! = 1*2*…*n-1*n");
        System.out.println(cyclesTask.factorialNumber(9));
        System.out.println("5. Посчитать сумму цифр заданного числа");
        System.out.println(cyclesTask.sumOfNumbersDigits(123));
        System.out.println("6. Вывести число, которое является зеркальным отображением последовательности цифр");
        System.out.println(cyclesTask.mirrorNumber(12345));

        System.out.println("\n" + "one dimensional arrays task" + "\n" + "-------------------------------");
        ArrayTask oneDiArrTask6 = new ArrayTask();
        System.out.println("1. Найти минимальный элемент массива");
        System.out.println(oneDiArrTask6.minArrNumber(new int[]{5,15,20,2,7,10}));
        System.out.println("2. Найти максимальный элемент массива");
        System.out.println(oneDiArrTask6.maxArrNumber(new int[]{5,15,20,2,7,10}));
        System.out.println("3. Найти индекс минимального элемента массива");
        System.out.println(oneDiArrTask6.indexOfMinArrNumber(new int[]{5,15,20,2,7,10}));
        System.out.println("4. Найти индекс максимального элемента массива");
        System.out.println(oneDiArrTask6.indexOfMaxArrNumber(new int[]{5,15,20,2,7,10}));
        System.out.println("5. Посчитать сумму элементов массива с нечетными индексам");
        System.out.println(oneDiArrTask6.sumOddArrNumber(new int[]{5,15,20,2,7,10}));
        System.out.println("6. Сделать реверс массива");
        System.out.println(Arrays.toString(oneDiArrTask6.reversArray(new int[]{1, 2, 3, 4})));
        System.out.println("7. Посчитать количество нечетных элементов массива");
        System.out.println(oneDiArrTask6.amountOddElements(new int[]{1, 2, 3, 4}));
        System.out.println("8. Поменять местами первую и вторую половину массива");
        System.out.println(Arrays.toString(oneDiArrTask6.changePartsArr(new int[]{1, 2, 3, 4})));
        System.out.println("9.1. Отсортировать массив пузырьком (Bubble)");
        System.out.println(Arrays.toString(oneDiArrTask6.bubbleSort(new int[]{5,15,20,2,7,10})));
        System.out.println("9.2 Отсортировать массив выбором (Select)");
        System.out.println(Arrays.toString(oneDiArrTask6.selectSort(new int[]{5,15,20,2,7,10})));
        System.out.println("9.3 Отсортировать массив вставками (Insert)");
        System.out.println(Arrays.toString(oneDiArrTask6.insertSort(new int[]{5,15,20,2,7,10})));

        System.out.println("\n" + "functions task" + "\n" + "-------------------------------");
        System.out.println("1. Получить строковое название дня недели по номеру дня");
        DayNameByNumber dayNameByNumber = new DayNameByNumber();
        System.out.println(dayNameByNumber.displayDay(6));
        System.out.println("2. Найти расстояние между двумя точками в двухмерном декартовом пространстве.");
        Pifagor pifagor = new Pifagor();
        System.out.println(pifagor.distanceBetweenPoints(-2,2,1,-2));
        System.out.println("3. Вводим число(0-999), получаем строку с прописью числа");
        NumberAsString userNumber = new NumberAsString();
        System.out.println(userNumber.getNumberValue(137));
        System.out.println(userNumber.getStringValue("четыреста двадцать семь"));

    }
}
