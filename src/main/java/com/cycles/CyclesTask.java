package main.java.com.cycles;

public class CyclesTask {

    public String sumNumbersAndQuantity() {
        int sum = 0;
        int count = 0;
        String sumNum = "";
        for (int i = 1; i < 100; i++) {
            if (i % 2 == 0) {
                sum += i;
                count += 1;

            }
        }
        sumNum = "Сумма чётных чисел - " + sum
                + " их количество: " + count;
        return sumNum;
    }

    public String simpleNumber(int number) {
        int count = 0;
        String result = "";
        for (int i = 1; i <= number; i++) {
            if ((number % i) == 0) {
                count += 1;
            }
        }
        if (count > 2) {
            result = "Число " + number + " не простое";
        } else {
            result = "Число " + number + " простое";
        }
        return result;

    }

    public int sqrtOfNumber(int number) {
        int result = 0;
        for (int i = 1; i < number; i++) {
            if (i * i == number) {
                result = i;
                break;
            }
            if (i * i > number) {
                result = --i;
                break;
            }
        }
        return result;
    }

    public int factorialNumber(int number) {
        int factorial = 1;
        for (int i = 1; i <= number; i++) {
            factorial *= i;
        }
        return factorial;
    }

    public int sumOfNumbersDigits(int number) {
        int sum = 0;
        int rest;
        while (number > 0) {
            rest = number % 10;
            sum += rest;
            number = (number - rest) / 10;
        }
        return sum;
    }

    public int mirrorNumber(int number) {
        int reversNumber = 0;
        while (number > 0) {
            reversNumber = reversNumber * 10 + number %10;
            number = number / 10;
        }
        return reversNumber;
    }
}




