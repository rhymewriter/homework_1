package main.java.com.functions;

public class Pifagor {
    public double distanceBetweenPoints(int x1, int y1, int x2, int y2){
        return Math.sqrt(Math.pow((x2-x1),2) + Math.pow((y2-y1),2));
    }
}
