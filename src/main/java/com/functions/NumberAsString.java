package main.java.com.functions;

import java.util.Arrays;

public class NumberAsString {
    public String[] units = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"};
    public String[] dozens = {"десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
    public String[] hundreds = {"сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
    public String[] uncommon = {"одинадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестьнадцать", "семьнадцать", "восемьнадцать", "девятьнадцать"};

    public NumberAsString() {
    }

    public String getNumberValue(int number) {
        String textNumber = "";
        int unit = number % 10;
        int dozen = number / 10 % 10;
        int hundred = number / 100;

        if (hundred > 0) {
            textNumber = hundreds[hundred - 1];
        }

        if (dozen > 0 && dozen != 1) {
            textNumber = textNumber + " " + dozens[dozen - 1];
        }
        if (unit > 0 && dozen != 1) {
            textNumber = textNumber + " " + units[unit];
        }
        if (unit == 0 && dozen == 0 && hundred == 0) {
            textNumber = textNumber + " " + units[unit];
        }
        if (unit > 0 && dozen == 1) {
            textNumber = textNumber + " " + uncommon[unit - 1];
        }
        if (unit == 0 && dozen == 1) {
            textNumber = textNumber + " " + dozens[unit];
        }
        return textNumber;
    }

    public int getStringValue(String stringNumber) {

        int finalNumber = 0;

        String[] stringNumbers = stringNumber.split(" ");

        if (stringNumbers.length == 3) {
            for (int i = 0; i < hundreds.length; i++) {
                if (stringNumbers[0].equalsIgnoreCase(hundreds[i])) {
                    finalNumber = (i + 1) * 100;
                }
            }
            for (int j = 0; j < dozens.length; j++) {
                if (stringNumbers[1].equalsIgnoreCase(dozens[j])) {
                    finalNumber += (j + 1) * 10;
                }
            }
            for (int k = 0; k < units.length; k++) {
                if (stringNumbers[2].equalsIgnoreCase(units[k])) {
                    finalNumber += k;
                }
            }
        }
        if (stringNumbers.length == 2) {
            for (int i = 0; i < hundreds.length; i++) {
                if (stringNumbers[0].equalsIgnoreCase(hundreds[i])) {
                    finalNumber = (i + 1) * 100;
                }

            }
            for (int j = 0; j < uncommon.length; j++) {
                if (stringNumbers[1].equalsIgnoreCase(uncommon[j])) {
                    finalNumber += j + 11;
                }
            }
            for (int k = 0; k < dozens.length; k++) {
                if (stringNumbers[0].equalsIgnoreCase(dozens[k])) {
                    finalNumber += (k + 1) * 10;
                }
            }

            for (int k = 0; k < units.length; k++) {
                if (stringNumbers[1].equalsIgnoreCase(units[k])) {
                    finalNumber += k;
                }
            }

        }
        if (stringNumbers.length == 1) {
            for (int i = 0; i < uncommon.length; i++) {
                if (stringNumbers[0].equalsIgnoreCase(uncommon[i])) {
                    finalNumber = i + 11;
                }
            }
            for (int j = 0; j < units.length; j++) {
                if (stringNumbers[0].equalsIgnoreCase(units[j])) {
                    finalNumber = j;
                }


            }


        }
        return finalNumber;

    }
}






