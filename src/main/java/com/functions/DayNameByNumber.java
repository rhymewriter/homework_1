package main.java.com.functions;

public class DayNameByNumber {

    int dayNumber = 0;
    String dayName = "";

    public String displayDay(int dayNumber) {
        if (dayNumber == 1) {
            dayName = "Понедельник";
            return dayName;
        } else if (dayNumber == 2) {
            dayName = "Вторник";
            return dayName;
        } else if (dayNumber == 3) {
            dayName = "Среда";
            return dayName;
        } else if (dayNumber == 4) {
            dayName = "Четверг";
            return dayName;
        } else if (dayNumber == 5) {
            dayName = "Пятница";
            return dayName;
        } else if (dayNumber == 6) {
            dayName = "Суббота";
            return dayName;
        } else if (dayNumber == 7) {
            dayName = "Воскресенье";
            return dayName;
        } else {
            dayName = "Некорректный ввод!!";
            return dayName;
        }

    }
}
