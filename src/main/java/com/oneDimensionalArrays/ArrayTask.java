package main.java.com.oneDimensionalArrays;

public class ArrayTask {

    public int minArrNumber(int[] arr){
        int min = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (min > arr[i]) {
                min = arr[i];
            }
        }
        return min;
    }

    public int maxArrNumber(int[] arr){
        int max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
            }
        } return max;
    }

    public int indexOfMinArrNumber(int[] arr){
        int indexMin = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[indexMin] > arr[i]) {
                indexMin = i;
            }
        }
        return indexMin;
    }

    public int indexOfMaxArrNumber(int[] arr){
        int indexMax = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[indexMax] < arr[i]) {
                indexMax = i;
            }
        } return indexMax;
    }

    public int sumOddArrNumber(int[] arr){
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 != 0) {
                sum += arr[i];
            }
        } return sum;
    }


    public int[] reversArray(int[] array) {
        if (array == null) {
            return null;
        }
        int[] revArr = new int[array.length];
        int count = 0;
        for (int i = array.length - 1; i >= 0; i--) {
            revArr[count] = array[i];
            count++;
        }
        return revArr;
    }

    public int amountOddElements(int[] array) {
        if (array == null) {
            return 0;
        }
        int count = 0;
        for (int element : array) {
            if (element % 2 != 0) {
                count++;
            }

        }
        return count;
    }

    public int[] changePartsArr(int[] array) {
        if (array == null) {
            return null;
        }
        int[] changePartsArray = new int[array.length];
        int count = 0;
        for (int i = array.length / 2; i < array.length; i++) {
            changePartsArray[count] = array[i];
            count++;
        }
        for (int i = 0; i < array.length / 2; i++) {
            changePartsArray[count] = array[i];
            count++;
        }
        return changePartsArray;
    }

    public int[] bubbleSort(int [] arr){
        int[] resultArr = java.util.Arrays.copyOf(arr,arr.length);
        boolean isSorted = false;
        int tmp;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < resultArr.length-1; i++) {
                if(resultArr[i] > resultArr[i+1]){
                    isSorted = false;
                    tmp = resultArr[i];
                    resultArr[i] = resultArr[i+1];
                    resultArr[i+1] = tmp;
                }
            }
        }
        return resultArr;
    }

    public int[] selectSort(int [] arr){
        int[] resultArr = java.util.Arrays.copyOf(arr,arr.length);
        int indexMin = 0;
        int min = 0;
        for (int i = 0; i < resultArr.length; i++) {
            indexMin = i;
            min = resultArr[i];
            for (int j = i + 1; j < resultArr.length; j++) {
                if (resultArr[j] < min) {
                    indexMin = j;
                    min = resultArr[j];
                }
            }
            resultArr[indexMin] = resultArr[i];
            resultArr[i] = min;
        }
        return resultArr;
    }

    public int[] insertSort(int [] arr){
        int[] resultArr = java.util.Arrays.copyOf(arr,arr.length);
        int current = 0;
        int j = 0;
        for (int i = 1; i < resultArr.length; i++) {
            current = resultArr[i];
            j = i - 1;
            while(j >= 0 && current < resultArr[j]) {
                resultArr[j+1] = resultArr[j];
                j--;
            }
            resultArr[j+1] = current;
        }
        return resultArr;
    }
}