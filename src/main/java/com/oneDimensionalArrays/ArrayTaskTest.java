package main.java.com.oneDimensionalArrays;

import org.junit.Assert;
import org.junit.Test;

public class ArrayTaskTest {

    @Test
    public void reversArray() {
        ArrayTask oneDiArrTask6 = new ArrayTask();
        int[] actual = oneDiArrTask6.reversArray(new int[]{1, 2, 3, 4});
        int[] expect = {4, 3, 2, 1};
        Assert.assertArrayEquals(expect, actual);
    }

    @Test
    public void reversArrayNull() {
        ArrayTask oneDiArrTask6 = new ArrayTask();
        int[] actual = oneDiArrTask6.reversArray(null);
        Assert.assertNull(actual);
    }

    @Test
    public void amountOddElements() {
        ArrayTask amountOddNumbers = new ArrayTask();
        int actual = amountOddNumbers.amountOddElements(new int[]{1, 2, 3, 4});
        int expect = 2;
        Assert.assertEquals(expect, actual);
    }

    @Test
    public void amountOddElementsNull() {
        ArrayTask amountOddNumbers = new ArrayTask();
        int actual = amountOddNumbers.amountOddElements(null);
        int expect = 0;
        Assert.assertEquals(expect, actual);
    }

    @Test
    public void changePartsArr() {
        ArrayTask changePaAr = new ArrayTask();
        int[] actual = changePaAr.changePartsArr(new int[]{1, 2, 3, 4, 5});
        int[] expect = {3, 4, 5, 1, 2};
        Assert.assertArrayEquals(expect, actual);
    }

    @Test
    public void changePartsArrNull() {
        ArrayTask changePaAr = new ArrayTask();
        int[] actual = changePaAr.changePartsArr(null);
        Assert.assertNull(actual);
    }
}
