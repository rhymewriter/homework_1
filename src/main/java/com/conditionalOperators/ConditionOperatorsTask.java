package main.java.com.conditionalOperators;

public class ConditionOperatorsTask {
    public ConditionOperatorsTask(){
    }

    public int getResult(int number, int number1) {
        return number % 2 == 0 ? number * number1 : number + number1;
    }

    public String getQuarterNumber(int x, int y) {
        String quarter = "";
        if(x > 0 && y > 0){
            quarter = "I";
        }
        else if(x < 0 && y > 0){
            quarter ="II";
        }
        else if(x < 0 && y < 0){
            quarter ="III";
        }
        else if(x > 0 && y < 0) {
            quarter = "IV";
        }
        return quarter;
    }

    public int getSumOfPositiveNumbers (int a, int b, int c) {
        int sum = 0;
        if (a > 0) {
            sum += a;
        }
        if (b > 0){
            sum +=b;
        }
        if (c > 0){
            sum +=c;
        }
        return sum;

    }

    public int getResultOfExpression (int a, int b, int c) {
        int result = 0;
        int d = 3;
        if((a * b * c) > (a + b + c)) {
            result = (a * b * c) + d;
        } else {
            result = (a + b + c) + d;
        }
        return  result;
    }
    public String getGrade (int rate) {
        String grade = "";
        if(rate >= 0 && rate <= 19) {
            grade = "F";
        } else if(rate >= 20 && rate <= 39) {
            grade = "E";
        } else if(rate >= 40 && rate <= 59) {
            grade = "D";
        } else if(rate >= 60 && rate <= 74) {
            grade = "C";
        } else if(rate >= 75 && rate <= 89) {
            grade = "B";
        } else if(rate >= 90 && rate <= 100) {
            grade = "A";
        } else {
            grade = "Wrong input";
        } return grade;
    }

}



